import React from'react';
import Landing from './landingPage';
import {Switch, Route} from 'react-router-dom';
import Project from './projects';
import About from './aboutMe';
import Resume from './resume';
import Contact from './contact';

const Main = () => (
    <Switch>
        <Route exact path="/" component={Landing} />
        <Route path="/about" component={About} />
        <Route path="/project" component={Project} />
        <Route path="/resume" component={Resume} />
        <Route path="/contact" component={Contact} />
    </Switch>
)

export default Main;